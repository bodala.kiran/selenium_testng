package testPac;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestNew {
  @Test(priority=1)
  public void modifyCustomer() {
	  System.out.println("The Customer Will Get Modified");
  
  }
  @Test(priority=3)
  public void createCustomer() {
	  System.out.println("The Customer Will Get Created");
	  
  }
  @Test(priority=2)
  public void newCustomer() {
	  System.out.println("New Customer Will Get Created");
  }
  @BeforeMethod
  public void beforeCustomer() {
	  System.out.println("Verify The Customer");
  }
  @AfterMethod
  public void afterCustomer() {
	  System.out.println("AllThe Transactions Are Done");
  }
  @BeforeClass
  public void beforeClass() {
	  System.out.println("Start the data base connection, launch browser");
	  
  }
  @AfterClass
  public void afterClass() {
	  System.out.println("Close the data base connection, Close browser");
	  
  }
}
